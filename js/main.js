'use strict'

var mapa = undefined;

$(document).ready( function(){

    mapa = new Mapa("map")

    var current = 1 //El numero de pagina donde estoy
    var current_step = undefined;
    var next_step = undefined;
    var steps = $("fieldset").length; //LA cantidad de paginas que hay

    var pedidos = [];
    var iPedidoSeleccionado = 0;
    var pedidoSeleccionado = undefined;    

    setProgressBar(current);
    cargarPedidos(); //Carga los pedidos de la api.

    $(".next").click(function(){
      //El boton esta en un button-group y el button-group en un fieldset (pagina del form)
      //Al hacer parent() "salgo" al button-group y al hacerlo otra vez "salgo" al fieldset.
      current_step = $(this).parent().parent(); //Obtiene el fieldset donde estoy parado.
      next_step = $(this).parent().parent().next(); //Obtiene el fieldset al donde me tengo que mover.
      next_step.show();
      current_step.hide();
      
      if ( next_step.has(".map-container").length == 1 ) //Si la pagina a la que me tengo que mover tiene un
                                                        //contenedor para el mapa...
        moveMap( next_step.children(".map-container") ); //Entonces lo muevo a ese contenedor.
      
      mapa.limpiarLayer("Pedido"); //Limpio el layer del pedido del mapa.
      $(next_step).trigger('cambioFieldset', pedidoSeleccionado); //Dispara un evento y pasa el pedido que se selecciono

      setProgressBar(++current); //Modifica el valor de la barra de progreso
    });

    $(".previous").click(function(){
      //El boton esta en un button-group y el button-group en un fieldset (pagina del form)
      //Al hacer parent() "salgo" al button-group y al hacerlo otra vez "salgo" al fieldset.
      current_step = $(this).parent().parent(); //Obtiene el fieldset donde estoy parado.
      next_step = $(this).parent().parent().prev(); //Obtiene el fieldset al donde me tengo que mover.
      next_step.show();
      current_step.hide();

      if ( next_step.has(".map-container").length == 1 )//Si la pagina a la que me tengo que mover tiene un
                                                        //contenedor para el mapa...
        moveMap( next_step.children(".map-container") ); //Entonces lo muevo a ese contenedor.
        
      mapa.limpiarLayer("Pedido");
      $(next_step).trigger('cambioFieldset', pedidoSeleccionado); //Dispara un evento y pasa el pedido que se selecciono

      setProgressBar(--current); //Modifica el valor de la barra de progreso
    });

    $('#descripcion').on('keydown', function(){
        //Cada vez que presiono una tecla sobre el campo con id descripcion swapeo el pedido seleccionado.
        if( iPedidoSeleccionado == 0 )
            iPedidoSeleccionado = 1;
        else
            iPedidoSeleccionado = 0;
        pedidoSeleccionado = pedidos[iPedidoSeleccionado];
    })

    $("fieldset").on('cambioFieldset', async function(eventInfo, pedidoSelecc){ //Al cambiar de fieldset (pagina)...
        if( $(this).attr('id') == "seleccRepartidor" ){//Checkeo que el fieldset (pagina del form) donde 
                                                      //estoy sea donde selecciono el repartidor, porque
                                                      //ahí es donde me interesa mostrar las cosas.
            
            //Ya checkeado que estoy en la que me interesa,
            //se llama a mostrarIncidentes() y mostrarRepartidores()
            mostrarIncidentes();
            mostrarRepartidores(pedidoSelecc);
        } else { //Caso contrario limpio los layers.
            mapa.limpiarLayer('Incidente');
            mapa.limpiarLayer('Repartidor');
        }
        
    })

    function setProgressBar(curStep){
      //El resultado de 100% dividido la cantidad de pesos lo multiplico 
      //por el numero de pagina actual y me da el porcentaje de progreso actual
      var percent = parseFloat(100 / steps) * curStep; 
      percent = percent.toFixed(); //Vuelvo entero al numero. toFixed() sin argumento 
                                //te devuelve un numero con 0 numeros despues de la coma
      $(".progress-bar") //Selecciono la barra de progreso
        .css("width", percent+"%") //Le modifico el porcentaje de progreso
        .html(percent+"%"); //Le pongo el numero en el html tambien para que se vea dentro de la barra
    }

    function moveMap(node){ //Mueve el mapa de nodo... Dah
        $("#map").detach().appendTo(node); //detach() quita el nodo pero no lo elimina
                                          //se lo guarda para insertarlo en otro lado.
    }

    function cargarPedidos(){
      $.ajax(`${urls.api_herokuapp}/requests/`) //Hace el request de pedidos a la api.
          .done( json => {
            pedidos = json.requests.map( request => { //Itera sobre la lista de pedidos del resultado para mapear
                  return new Pedido(request); //En el mapeo se encarga de convertir cada pedido 
                                            //en formato json a una instancia de la clase Pedido
            });
            pedidoSeleccionado = pedidos[0]; //EL pedido seleccionado por defecto es el de indice 0.
          })
    }

});