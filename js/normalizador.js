'use strict'

var direcciones = []

$(document).ready(function(e) {

    //Cuando el usuario termina de escribir muestra el menu desplegable
    $(".dropdown-toggle").on('keyup', function(){

        if( e.keyCode == 13 ) //keyCode 13 es el Enter
            e.preventDefault(); //Evito que se buguee cuando apreten Enter

        //dropdown_id = origen o destino
        var dropdown_id = $(this).attr('id'); //Obtengo el id del campo para buscar el menu
        var dropdown_menu = $(`div[aria-labelledby=\"${dropdown_id}\"]`); //menu desplegable a mostrar
        $(dropdown_menu).find('a[class="dropdown-item"]').remove(); //Limpio el menu de resultados anteriores

        if( $(this).val() == "" ) //Si el campo de texto esta vacio se debe cerrar el menu desplegable...
            $(dropdown_menu).removeClass("show"); //Con esto no se muestra

        else {
            var url = `${urls.api_normalizacion}/?direccion=${$(this).val()}&maxOptions=5`
            fetch(url).then(response => { //Le pego a la api con un maximo de 5 resultados.
                return response.json();
            }).then(json => {
                if( json.errorMessage ){ //Si dio un error
                    $(dropdown_menu).find('a[class="dropdown-item"]').remove(); //Limpio el menu de resultados anteriores
                    $(dropdown_menu).append(`<a class='dropdown-item'>${json.errorMessage}</a>`); //Muestro el error en el menu
                } else {
                    $(dropdown_menu).find('a[class="dropdown-item"]').remove(); //Limpio el menu de resultados anteriores
                    direcciones = json.direccionesNormalizadas; //Me guardo el resultado
                    json.direccionesNormalizadas.forEach( direccion => {  //Recorro las direcciones que trajo la api                  
                        /* Inicio creacion elemento para arregarlo al menu */
                        var elem = document.createElement("a");
                        $(elem)
                            .addClass("dropdown-item")
                            .attr("cod_calle", direccion.cod_calle) //Le agrego este atributo para vincularlos para luego
                            .text(direccion.direccion)
                            .on('click', selecccionarDireccion);
                        /* Fin creacion elemento para arregarlo al menu */
                        $(dropdown_menu).append(elem); //Lo agrego al menu
                    });
                }
                
            })
            
            $(dropdown_menu).addClass("show"); //Con esto se muestra
        }
            
    });

    $(".dropdown-toggle").on('keydown', function(){

        if( e.keyCode == 13 ) //keyCode 13 es el Enter
            e.preventDefault(); //Evito que se buguee cuando apreten Enter

        var dropdown_id = $(this).attr('id'); //Obtengo el id del campo para buscar el menu
        var dropdown_menu = $(`div[aria-labelledby=\"${dropdown_id}\"]`); //Menu desplegable a mostrar
        $(dropdown_menu).find('a[class="dropdown-item"]').remove(); //Limpio el menu. 
                                                        //Se hace aca también para evitar el bug de repeticiones
    });

    //Detecta si se hizo click fuera del menu desplegable.
    //En caso de que si entonces lo oculta, si no, pos no.
    $(document).on('click', function(e){
        var dropdowns = $(".dropdown-menu"); //Obtengo todos los dropdowns
        if ( !dropdowns.is(e.target) && dropdowns.has(e.target).length === 0 )//Checkeo si el click fue dentro
            dropdowns.removeClass("show");                           //del dropdown o alguno de sus nodos hijos
    });

    //Setea el item seleccionado del menu en el campo de texto
    function selecccionarDireccion(){
        //dropdown_id = origen o destino
        var dropdown_id = $(this).parent().attr("aria-labelledby"); //Del item del menu "vuelvo" al campo de texto
        var dropdown_field = $(`#${dropdown_id}`); //Campo de texto
        var selected_item_text = $(this).text();
        var dropdown_menu = $(`div[aria-labelledby=\"${dropdown_id}\"]`);
        $(dropdown_menu).removeClass('show'); //Oculto el menu desplegable
        dropdown_field.val(selected_item_text); //Al campo de texto le seteo el texto del item seleccionado.

        var calle = $(this).attr('cod_calle'); //Obtengo el cod_calle del item seleccionado
        for(var i = 0; i< direcciones.length;i++){
            if(calle == direcciones[i].cod_calle){ //Busco en las direcciones guardadas cual es la que tiene
                                                    //el mismo cod_calle para poder dibujarla en el mapa
                if (dropdown_id == "origen") { //Si es el origen...
                    mapa.limpiarLayer('Origen'); //Borro el origen anterior 
                    //Lo dibujo
                    mapa.dibujarMarker({ lat: direcciones[i].coordenadas.y, lon: direcciones[i].coordenadas.x }, "Origen", capitalizar(dropdown_id), true);
                    //Centro la vista
                    mapa.centrar({ lat: direcciones[i].coordenadas.y, lon: direcciones[i].coordenadas.x })
                } else if (dropdown_id == "destino") { //Si es el destino...
                    mapa.limpiarLayer('Destino'); //Borro el destino anterior
                    //Lo dibujo
                    mapa.dibujarMarker({ lat: direcciones[i].coordenadas.y, lon: direcciones[i].coordenadas.x }, "Destino", capitalizar(dropdown_id), true); 
                    //Centro la vista
                    mapa.centrar({ lat: direcciones[i].coordenadas.y, lon: direcciones[i].coordenadas.x })
                }
            }
        }


    }
});