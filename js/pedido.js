'use strict'

class Pedido {

    constructor(json){

        this.id = json.id;
        this.sender = json.sender;
        this.receiver = json.receiver;
        this.availableDrivers = json.availableDrivers;

    }

}