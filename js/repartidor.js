'use strict'

class Repartidor {

    constructor(json){

        // -- Atributos de la clase --
        this.id = json.id;
        this.name = json.name;
        this.surname = json.surname;
        this.score = json.score;
        this.car = json.car;
        this.positions = undefined;    
        this.iterador = undefined;  
        // -- Fin atributos de la clase --

    }

    * __crearIterador() {
        let i = 0;
        do {
            yield this.positions[i++]; //Devuelve la posicion en el indice i y pausa el
                                                //hilo de ejecución donde esta corriendo este iterador
                                                //(No detiene la UI). Cuando continua la ejecutación 
                                                //aumenta el contador y sigue con el ciclo.
                                                //Es como re loco, a la primera no lo entendí...
                                                //Pero esta epico.
            if( i >= this.positions.length )
                i = 0;
        } while (true);
    }

    setPositions(arr){
        //Setea las posiciones y crea un iterador
        this.positions = arr;
        this.iterador = this.__crearIterador();
    }

    obtenerPosicion(){
        if( !this.positions )
            throw new Error("No se setearon posiciones. Use instancia.setPostitions(arr)");
        return this.iterador.next().value; //Devuelve el siguiente valor del iterador.
    }

}