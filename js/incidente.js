'use strict'

class TipoIncidente {
    
    constructor(json){
        
        this.id = json.id;
        this.description = json.description;
        this.delay = json.delay;

    }    

}

class Incidente {
    
    constructor(json){

        this.id = json.id;
        this.coordinate = json.coordinate;
        this.date = json.date;
        this.type_id = json.type_id;
        this.type = undefined;

    }

    setType(json){
        this.type = new TipoIncidente(json);
    }
    
}