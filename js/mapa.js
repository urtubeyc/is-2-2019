'use strict'

var mapa = undefined;

var estilosMarkers = { //Hay que escribirlos.
    'Origen': {
        icon: greenIcon 
    },
    'Destino': {
        icon: blueIcon
    },
    'Incidente': {
        icon: redIcon
    },
    'Repartidor': {
        icon: orangeIcon
    },
}

var layers = {
    'Origen': null,
    'Destino':null,
    'Repartidor': null,
    'Incidente': null
}

var overlays = {
    'Origen': '<img src="./css/ext/images/marker-icon-green.png" style="transform: scale(0.5, 0.5)"> <td style="vertical-align: middle; font-family:\'Open Sans\', sans-serif; color: #4f5f6f;">Origen</td>',
    'Destino': '<img src="./css/ext/images/marker-icon-blue.png" style="transform: scale(0.5, 0.5)"> <td style="vertical-align: middle; font-family:\'Open Sans\', sans-serif; color: #4f5f6f;">Entrega</td>',
    'Incidente': '<img src="./css/ext/images/marker-icon-red.png" style="transform: scale(0.5, 0.5)"> <td style="vertical-align: middle; font-family:\'Open Sans\', sans-serif; color: #4f5f6f;">Incidente</td>',
    'Repartidor': '<img src="./css/ext/images/marker-icon-orange.png" style="transform: scale(0.5, 0.5)"> <td style="vertical-align: middle; font-family:\'Open Sans\', sans-serif; color: #4f5f6f;">Repartidor</td>'
}

class Mapa {

    constructor(idNodo){

        this.mapa = L.map(idNodo).setView([-34.5221554, -58.7000067], 13);

        var baseLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.mapa);

        var layersControl = L.control.layers({
            "Referencias": baseLayer
        }).addTo(this.mapa);
        
        this.mapa.layersControl = layersControl;

        for (var layerName in layers) {
            var layer = L.layerGroup();
            layer.id = layerName;
            layers[layerName] = layer;
            layer.addTo(this.mapa);
            this.mapa.layersControl.addOverlay(layers[layerName], overlays[layerName]);
        }

    }

    dibujarPedido(pedido){
        layers["Origen"].clearLayers();
        layers["Destino"].clearLayers();
        this.dibujarMarker(pedido.sender, 
                            "Origen",
                            "Punto de origen pedido");
        this.dibujarMarker(pedido.receiver, 
            "Destino",
            'Punto de entrega pedido');
    }

    dibujarIncidente(incidente){
        this.dibujarMarker(incidente.coordinate, 
                "Incidente",
                `${capitalizar(incidente.type.description)} | Demora: ${incidente.type.delay}min`
        );
    }

    dibujarRepartidor(repartidor){
        var markerRepartidor = this.dibujarMarker(repartidor.obtenerPosicion(), "Repartidor", repartidor.name+"  "+repartidor.surname+" | Puntaje: "+repartidor.score);
        markerRepartidor.repartidor_asociado = repartidor;
        markerRepartidor.mover = function(){
            setInterval( function(){
                markerRepartidor.setLatLng( markerRepartidor.repartidor_asociado.obtenerPosicion() );
            }, 1000 );
        }
        return markerRepartidor;
    }

    dibujarMarker(pos, layerName, popMsg, showPop){
        var marker = L.marker([pos.lat, pos.lon], estilosMarkers[layerName])
                        .bindPopup(popMsg)
                        .addTo(layers[layerName]);
        if( showPop )
            marker.openPopup();
        return marker;
    }

    quitarMarker(nombreLayer, marker){
        if( nombreLayer == 'Pedido')
            if( layers['Origen'].hasLayer(marker) )
                nombreLayer = 'Origen';
            else
                nombreLayer = 'Destino';
        layers[nombreLayer].removeLayer(marker);
    }

    limpiarLayer(nombreLayer){
        if( nombreLayer == "Pedido" ){
            layers['Origen'].clearLayers();
            layers['Destino'].clearLayers();
        } else
            layers[nombreLayer].clearLayers();
    }

    moverRepartidor(markerRepartidorSeleccionado){
        this.limpiarLayer('Repartidor');
        markerRepartidorSeleccionado.addTo(layers['Repartidor']);
        markerRepartidorSeleccionado.mover();
    }

    centrar(pos){
        this.mapa.setView(pos);
    }

}