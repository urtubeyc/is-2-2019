'use strict'

function mostrarIncidentes(){
    
    var incidentes = [];
        
    $.ajax( `${urls.api_herokuapp}/incidents/` ) //Hace un request a la API para pedir los incidentes
        .done( json => {  //Una vez que termina...
            incidentes = json.incidents; //Se guarda los incidentes del JSON una variable
            incidentes = incidentes.map(incidente => { //Mapea cada incidente, hasta ahora estan como JSON                        
                return new Incidente(incidente); //Pero aca lo devuelo como un objeto de tipo Incidente
            });

            incidentes.forEach(incidente => { //Recorro la lista de incidentes...
                $.ajax(`${urls.api_herokuapp}/incidenttypes/${incidente.type_id}`) //De cada incidente
                                                        //obtengo el ID del tipo y consulto en API por el
                                                        //tipo de incidente.
                    .done( json => {
                        incidente.setType( json.incidenttype ); //Le seteo el tipo de incidente al incidente
                        mapa.dibujarIncidente(incidente); //...Y lo dibujo en el mapa
                    });
            });

        });

}