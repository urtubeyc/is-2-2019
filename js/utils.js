'use strict'

//Pone la primer letra de cada palabra en mayusculas
function capitalizar(cadena){
    var palabras = cadena.split(" "); //Hace un split de la cadena con el espacio, se guarda un arreglo
    palabras = palabras.map( palabra => {  //Recorre el arreglo de las palabras spliteadas para mapear...
        //Devuele el primer caracter en mayuscula + todos los caracteres desde el indice 1 hasta el final en mayuscula
        return palabra.charAt(0).toUpperCase() + palabra.slice(1).toLowerCase();
    });
    return palabras.join(" "); //Vuelve a juntar la cadena.
}