'use strict'

//Los markers de colores para el mapa.

var blueIcon = new L.Icon({
    iconUrl: 'css/ext/images/marker-icon-blue.png',
    shadowUrl: 'css/ext/images/marker-shadow.png'
});

var redIcon = new L.Icon({
    iconUrl: 'css/ext/images/marker-icon-red.png',
    shadowUrl: 'css/ext/images/marker-shadow.png'
});

var greenIcon = new L.Icon({
    iconUrl: 'css/ext/images/marker-icon-green.png',
    shadowUrl: 'css/ext/images/marker-shadow.png'
});

var orangeIcon = new L.Icon({
    iconUrl: 'css/ext/images/marker-icon-orange.png',
    shadowUrl: 'css/ext/images/marker-shadow.png'
});

var yellowIcon = new L.Icon({
    iconUrl: 'css/ext/images/marker-icon-yellow.png',
    shadowUrl: 'css/ext/images/marker-shadow.png'
});

var violetIcon = new L.Icon({
    iconUrl: 'css/ext/images/marker-icon-violet.png',
    shadowUrl: 'css/ext/images/marker-shadow.png'
});

var greyIcon = new L.Icon({
    iconUrl: 'css/ext/images/marker-icon-grey.png',
    shadowUrl: 'css/ext/images/marker-shadow.png'
});

var blackIcon = new L.Icon({
    iconUrl: 'css/ext/images/marker-icon-black.png',
    shadowUrl: 'css/ext/images/marker-shadow.png'
});
