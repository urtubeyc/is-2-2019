'use strict'

var markerRepSeleccionado = undefined; //El marker de repartidor que fue seleccionado

function mostrarRepartidores(pedido) {

    mapa.dibujarPedido(pedido); //Dibujo el pedido en el mapa
    $("#listaRepartidores").find(".repartidor").remove(); //Limpio el listado de repartidores.

    pedido.availableDrivers.forEach(driver => { //Recorre la lista de repartidores disponibles para el pedido que llego
        $.ajax(`${urls.api_herokuapp}/deliverydrivers/${driver.driver_id}`) //Hace un request a la api
                                                        //para obtener los datos completos del repartidor
                                                        //usando los IDs que tiene el pedido
            .done( json => {
                var repartidor = new Repartidor(json.driver);
                $.ajax(`${urls.api_herokuapp}/deliverydrivers/${driver.driver_id}/positions/`)
                                        //Hace un request a la api pidiendo las posiciones del repartidor
                    .done( jsonPos => {
                        repartidor.setPositions(jsonPos.positions) //Setea las posiciones al repartidor
                        var marker = mapa.dibujarRepartidor(repartidor); //Dibuja el repartidor en el mapa

                        /* Inicio creación item para el listado de repartidores */
                        var elem = document.createElement("a"); 
                        $(elem)
                            .addClass("list-group-item")
                            .addClass("repartidor")
                            .attr("id_repartidor", repartidor.id)
                            .text(`${repartidor.name} ${repartidor.surname} | ${repartidor.car.description} - Patente: ${repartidor.car.plateNumber} | Puntaje: ${repartidor.score}/5`);
                        /* Fin creación item para el listado de repartidores */
                        $("#listaRepartidores").append(elem); //Lo agrega al listado
                        marker.on('click', function(){ //Al hacer click sobre un marker de repartidor...
                            //Si el que se hizo click fue previamente clickeado los deselecciona...
                            if( markerRepSeleccionado && this.repartidor_asociado.id == markerRepSeleccionado.repartidor_asociado.id ){
                                markerRepSeleccionado = undefined; //Lo desreferencia...
                                $(".item-selected").removeClass("item-selected"); //...Le quita la clase que 
                                                                        //da el color de "seleccionado" del listado
                            } else { //Caso contrario
                                markerRepSeleccionado = this; //Se guarda la referencia del marker
                                $(".item-selected").removeClass("item-selected"); //Al que estaba con la clase de
                                                                    //color "seleccionado" se la quita...
                                $(`a[id_repartidor=\"${markerRepSeleccionado.repartidor_asociado.id}\"]`)
                                    .addClass("item-selected") //...Y se lo pone al nuevo
                            }
                        });
                    });
            });
        });
}

$(document).ready( function(){

    $("#confirmRepartidor").on('click', function(){ //Al hacer click en el boton confirmar...
        if( !markerRepSeleccionado ) //Si no hay ningun repartidor seleccionado...
            $("#errorSeleccion").show(); //Muestra un error.
        else
            mapa.moverRepartidor(markerRepSeleccionado); //Caso contrario, lo mueve.
    });

    $("button[data-dismiss='modal']").on('click', function(){ //Si hace click en el boton cerrar del msg de error
        $(".modal").hide(); //...Cierra el msg de error
    });

});